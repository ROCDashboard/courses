class Course < ApplicationRecord
  belongs_to :parent, class_name: "Course", optional: true
  has_many :children, class_name: "Course", foreign_key: :parent_id
  has_many :grandchildren, class_name: "Course", through: :children, source: :children
  has_many :greatgrandchildren, class_name: "Course", through: :children, source: :grandchildren

  has_many :enrollments
  has_many :users, through: :enrollments

end
