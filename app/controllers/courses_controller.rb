class CoursesController < ApplicationController
  def index
    @courses = Course.includes(:children).where("parent_id is NULL")
  end

  def new
    authorize! :new, @course
    @course = Course.new
    @categories = Course.all
  end

  def create
    course = Course.new(course_params)
    # Course.create!(course_params)
    if course.save!
        redirect_to courses_path, notice: 'Created Successfully'
    else 
      redirect_to new_course_path, alert: 'Unable to create course'
    end    
  end

  def show
    @category_courses = Course.includes(:children).where(parent_id: params[:id])
  end
 
  private
  def course_params
    params.require(:course).permit(:name, :parent_id, :description)
  end
end
