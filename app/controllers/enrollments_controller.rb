class EnrollmentsController < ApplicationController
  
  def index
    @user_courses = current_user.courses
  end

  def create
    if Enrollment.exists?(user_id: current_user.id,course_id: params[:course_id])
      redirect_to courses_path, alert: 'Already Enrolled!'
    else
      @enrollment = Enrollment.create!(user_id: current_user.id,course_id: params[:course_id])
      redirect_to courses_path, notice: 'Enrolled Successfully'
    end  
  end

  def register
    @course = Course.find(params[:course_id])
  end

end