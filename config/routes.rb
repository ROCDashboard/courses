Rails.application.routes.draw do
  devise_for :users
  resources :courses, only: [:new,:create,:index,:show]
  resources :enrollments, only: [:index,:create]
  get '/register', to: 'enrollments#register'
  root to: 'courses#index'
end
