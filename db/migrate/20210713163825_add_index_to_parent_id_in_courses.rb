class AddIndexToParentIdInCourses < ActiveRecord::Migration[6.1]
  def change
    add_index :courses, :parent_id
  end
end
