class AddDescriptionForCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :description, :string, default: ''
  end
end
